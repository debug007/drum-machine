### STAGE 1: Build ###
FROM node as build

WORKDIR /work

COPY package*.json ./
RUN echo '{ "allow_root": true }' > /root/.bowerrc
RUN npm install 

COPY . .
RUN npm run build
RUN npm run test

### STAGE 2: Production Environment ###
FROM nginx
COPY --from=build /work/public /usr/share/nginx/html

